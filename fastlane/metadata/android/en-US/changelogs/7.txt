Since last update:
- Using Gradle 5.0!
- Migrated "app" to "appCompat"
- Migrated to AndroidX
- Updated BottomBar library fork to v2.5.0 (migration to AndroidX)
- Delete contributor strings from the app (and put them in the README.md of the project), which will hopefully shrink the APK size a bit